/**
 * 
 */
package com.main.uiautomation.configuration.browser;

public enum BrowserType {
	Firefox,
	Iexplorer,
	HtmlUnitDriver,
	Chrome
}
