# Enterprise automation
Tools and Technology used:
1. Language - Java
2. Build Tool - Maven
3. Test framework - Selenium
4. Logging Framework - Log4j
5. Reporting Framework - Extend Reports
6. Framework - Customized Framework build with TestNG framework as base
7. Framework Type - Data Driven
8. Version control system - Git

Features implemented in framework:
Some of the features implemented in framework are listed below.
1. POM - Page Object Model design pattern is implemented
2. Page Factory is used
3. pom.xml is used to manage all dependencies
4. testNG.xml is configured to manage test runs
5. Log4j is used for logging purpose and output file can be found at path target/automation.out
6. Extend Reports are used for reporting purpose and generated html report can be found at path Test_Execution_Results/Results_{TimeStamp}/Response_Report_{TimeStamp}.html
7. This framework supports Windows/Linux/Mac OS which will be detected automatically and Chrome/IE/Firefox/HTMLUnit browsers which can be configured from config.properties file under src/main/resources/configfile
8. This is data driven and the input can be configured in Excels stored under  src/main/resources /TestData. The excel row has RunMode column which provides flexibility in case user doesn't want to run the same test for few test data stored.
9. Test Data provider concept is used and parameters are stored in excel. The same test will run for the no. of times the rows has data in excel + its RunMode column has value 'Yes'
10. The framework will take screenshot for all failed test cases by default and can be extended for all pass cases on need. The screenshot will be attached to html report + will be stored under Test_Execution_Results/Results_{TimeStamp}/Screenshots
11. All useful and reusable functions are provided under Helper classes to make code completion time faster + error free like DropdownHelper, GenericHelper, BrowserHelper, WaitHelper etc
