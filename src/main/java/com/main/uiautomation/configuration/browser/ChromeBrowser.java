/**
 * 
 */
package com.main.uiautomation.configuration.browser;

import java.util.HashMap;
import java.util.Map;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.main.uiautomation.Utility.ResourceHelper;

public class ChromeBrowser {

	public WebDriver getChromeDriver() {
		if (System.getProperty("os.name").contains("Mac")){
			WebDriverManager.chromedriver().setup();
			return new ChromeDriver();
		}
		else if(System.getProperty("os.name").contains("Window")){
			WebDriverManager.chromedriver().setup();
			return new ChromeDriver();
		}
		else if(System.getProperty("os.name").contains("Linux")){
			WebDriverManager.chromedriver().setup();
			return new ChromeDriver();
		}
		return null;
	}
}
