
package com.main.uiautomation.helper.TestBase;

import java.io.File;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.NoSuchSessionException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;


import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.main.uiautomation.Utility.DateTimeHelper;
import com.main.uiautomation.Utility.ResourceHelper;
import com.main.uiautomation.Utility.UtilityHelper;
import com.main.uiautomation.configreader.ObjectRepo;
import com.main.uiautomation.configreader.PropertyFileReader;
import com.main.uiautomation.configuration.browser.BrowserType;
import com.main.uiautomation.configuration.browser.ChromeBrowser;
import com.main.uiautomation.configuration.browser.FirefoxBrowser;
import com.main.uiautomation.configuration.browser.HtmlUnitBrowser;
import com.main.uiautomation.configuration.browser.IExploreBrowser;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.*;

public class TestBase {

	private static Logger log;
	public static WebDriver driver;
	public String screenshotPath;
	public static ExtentReports extent = new ExtentReports();
	public static ExtentTest xtReportLog = null;
	public static ExtentHtmlReporter htmlReporter = null;
	public static List<Map<String, String>> dataItem = new ArrayList<>();
	public static int dataCounter = 0;
	public static int currentData = 0;
	public static String previousMethodName;
	public static Map<String, String> data = new HashMap<>();
	ExtentTest parentLog = null;
	public static Markup strcode = MarkupHelper.createCodeBlock("text");
	public static String WriteResultToExcel = "No";
	/*public static final String sdft = new SimpleDateFormat("YYYY-MM-dd_HHmmss").format(new Date());
	public static String resultsDIR = "Test_Execution_Results" + File.separator + "Results"
			+ sdft;*/

	public static String resultsDIR = "Test_Execution_Results" + File.separator + "Results"
			+ UtilityHelper.getCurrentDateTime();

	public static void setDataItem(List<Map<String, String>> dataItem) {
		TestBase.dataItem = dataItem;
		dataCounter = dataItem.size();
		currentData = 0;
	}

	@BeforeSuite
	public void startReport() {
		String sdft = new SimpleDateFormat("YYYY-MM-dd_HHmmss").format(new Date());
		ObjectRepo.reader = new PropertyFileReader();
		String log4jConfPath = ResourceHelper.getResourcePath("\\src\\main\\resources\\configfile\\log4j.properties");
		PropertyConfigurator.configure(log4jConfPath);
		log = Logger.getLogger(TestBase.class);
		UtilityHelper.CreateADirectory(resultsDIR);
		htmlReporter = new ExtentHtmlReporter(ResourceHelper.getBaseResourcePath() + File.separator + resultsDIR
				+ File.separator + "Response_Report" + UtilityHelper.getCurrentDateTime() + ".html");
		screenshotPath =  resultsDIR + File.separator
				+ "Screenshots";
		UtilityHelper.CreateADirectory(screenshotPath);

		htmlReporter.setAppendExisting(true);
		htmlReporter.config().setChartVisibilityOnOpen(true);

		htmlReporter.loadXMLConfig(new File(
				ResourceHelper.getBaseResourcePath() + "\\src\\main\\resources\\configfile\\extent-config.xml"));

		init();

		extent.attachReporter(htmlReporter);
		extent.setSystemInfo("User Name", System.getProperty("user.name"));
		extent.setSystemInfo("OS", System.getProperty("os.name"));
		extent.setSystemInfo("Environment", ObjectRepo.reader.getApplication());
		extent.setSystemInfo("Browser", ObjectRepo.reader.getBrowser().toString());
		extent.setSystemInfo("Host Name", "Automation_Team");
		extent.setReportUsesManualConfiguration(true);
	}

	@BeforeClass
	public void beforeClass() {
		try {
			parentLog = extent.createTest((this.getClass().getSimpleName()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		currentData = 0;
	}

	@BeforeMethod()
	public void beforeMethod(Method method) {
		gotoHomePage();
		if (dataItem.size() != 0 || !dataItem.isEmpty()) {
			data.putAll(dataItem.get(currentData++));
			dataCounter = dataItem.size();
		}
		log.info("******************************************************************************");
		log.info("*********" + this.getClass().getPackage().toString()
				.substring(this.getClass().getPackage().toString().lastIndexOf(".") + 1) + "::"
				+ this.getClass().getSimpleName() + "::" + method.getName() + "*********");
		if (currentData == 1 || currentData == 0) {
			previousMethodName = method.getName();
			xtReportLog = parentLog.createNode(method.getName());
		}

		if (dataCounter != 0 && !method.getName().equals(previousMethodName)) {
			xtReportLog = parentLog.createNode(method.getName());
		}

		if (dataCounter != 0 && dataCounter >= 2)
			xtReportLog = xtReportLog.createNode(data.get("Test_Description"))
					.assignCategory(this.getClass().getPackage().toString()
							.substring(this.getClass().getPackage().toString().lastIndexOf(".") + 1));
		else {
			xtReportLog = xtReportLog.assignCategory(this.getClass().getPackage().toString()
					.substring(this.getClass().getPackage().toString().lastIndexOf(".") + 1));
		}
		previousMethodName = method.getName();

	}

	@AfterMethod()
	public void afterMethod(ITestResult result) {
		try {
			getresult(result);
			extent.flush();
		} catch (Exception e) {
			Reporter("Exception in @AfterMethod: " + e, "Error", log);
		}
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		tearDownDriver();
		if (dataItem.size() != 0 || !dataItem.isEmpty()) {
			dataItem.clear();
			dataCounter = 0;
			data.clear();
		}
	}

	public void tearDownDriver() {
		if (((RemoteWebDriver) driver).getSessionId() != null) {
			System.out.println("Tearing down driver...." + ((RemoteWebDriver) driver).getSessionId().toString());
			quitDriver();
		}
	}

	public void quitDriver() {
		driver.quit();
		driver = null;
	}

	public void gotoHomePage() {
		try {
			driver.get(ObjectRepo.reader.getApplication());
			driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
		} catch (Exception e) {
			System.out.println(" Driver object exception. Creating new session and closing existing one");
			reinit();
			gotoHomePage();
		}
	}



	public void reinit() {

		try {
			driver.quit();
		} catch (Exception e) {

		}
		init();

	}


	public void init() {
		try {
			ObjectRepo.reader = new PropertyFileReader();
			log.info(ObjectRepo.reader.getBrowser());
			setUpDriver(ObjectRepo.reader.getBrowser());
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		} catch (Exception e) {
			Reporter("Exception while initlization :" + e.getMessage(), "Error");
		}
	}

	public void setCurrentData() {
		currentData = 0;
	}

	public void setUpDriver(BrowserType bType) {

		try {
			driver = getBrowserObject(bType);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public WebDriver getBrowserObject(BrowserType bType) throws Exception {
		try {
			switch (bType) {

			case Chrome:
				ChromeBrowser chrome = ChromeBrowser.class.newInstance();
				return chrome.getChromeDriver();

			case Firefox:
				FirefoxBrowser firefox = FirefoxBrowser.class.newInstance();
				return firefox.getFirefoxDriver();

			case HtmlUnitDriver:
				HtmlUnitBrowser htmlUnit = HtmlUnitBrowser.class.newInstance();
				return htmlUnit.getHtmlUnitDriver();

			case Iexplorer:
				IExploreBrowser iExplore = IExploreBrowser.class.newInstance();
				return iExplore.getIExplorerDriver();
			default:
				throw new Exception(" Driver Not Found : " + new PropertyFileReader().getBrowser());
			}
		} catch (Exception e) {
			log.equals(e);
			throw e;
		}
	}

	/**
	 * 
	 * @param result
	 */
	public void getresult(ITestResult result) {
		try {
			File screenShotName;
			String sdft = new SimpleDateFormat("YYYY-MM-dd_HHmmss").format(new Date());
			if (result.getStatus() == ITestResult.SUCCESS) {
				xtReportLog.log(Status.PASS, result.getName() + " test is pass");
			} else if (result.getStatus() == ITestResult.SKIP) {
				log.debug(result.getName() + " test is skipped and skip reason is:-" + result.getThrowable());
			} else if (result.getStatus() == ITestResult.FAILURE) {
				String scrPath = takeScreenShot(result.getName() + "_Fail");
				xtReportLog.fail(result.getName() + " test is failed",
						MediaEntityBuilder.createScreenCaptureFromPath(scrPath).build());
				log.error(result.getName() + " test is failed" + result.getThrowable());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public String takeScreenShot(String name) {
		try {
			File screenShotName;
			File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			String scrPath = ResourceHelper.getBaseResourcePath() + File.separator + screenshotPath + File.separator + name + ".png";
			screenShotName = new File(scrPath);
			FileUtils.copyFile(scrFile, screenShotName);
			String filePath = screenShotName.toString();
			return filePath;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	/**
	 * This method is used for reporting in extent report
	 * 
	 * @param text
	 * @param status
	 */
	public void Reporter(String text, String status) {
		if (status.equalsIgnoreCase("Pass")) {
			xtReportLog.log(Status.PASS, text);
			log.info(text);
		} else if (status.equalsIgnoreCase("Fail")) {
			xtReportLog.log(Status.FAIL, text);
			log.error(text);
		} else if (status.equalsIgnoreCase("Fatal")) {
			xtReportLog.log(Status.FATAL, text);
			log.fatal(text);
		} else if (status.equalsIgnoreCase("Error")) {
			xtReportLog.log(Status.FATAL, text);
			log.fatal(text);
		} else {
			xtReportLog.log(Status.INFO, text);
			log.info(text);
		}
		Reporter.log(text);
	}

	public void Reporter(String text, String status, Logger log) {
		if (status.equalsIgnoreCase("Pass")) {
			xtReportLog.log(Status.PASS, text);
			log.info(text);
		} else if (status.equalsIgnoreCase("Fail")) {
			xtReportLog.log(Status.FAIL, text);
			log.error(text);
		} else if (status.equalsIgnoreCase("Skip")) {
			xtReportLog.log(Status.SKIP, text);
			log.debug(text);
		} else if (status.equalsIgnoreCase("Fatal")) {
			xtReportLog.log(Status.FATAL, text);
			log.fatal(text);
		} else if (status.equalsIgnoreCase("Error")) {
			xtReportLog.log(Status.FATAL, text);
			log.fatal(text);
		} else {
			xtReportLog.log(Status.INFO, text);
			log.info(text);
		}
		Reporter.log(text);
	}

	public void ReporterForCodeBlock(String text, String status) {

		Markup code = MarkupHelper.createCodeBlock(text);
		if (status.equalsIgnoreCase("Pass")) {
			xtReportLog.log(Status.PASS, code);
			log.info(text);
		} else if (status.equalsIgnoreCase("Fail")) {
			xtReportLog.log(Status.FAIL, code);
			log.error(text);
		} else if (status.equalsIgnoreCase("Fatal")) {
			xtReportLog.log(Status.FATAL, text);
			log.fatal(text);
		} else if (status.equalsIgnoreCase("Error")) {
			xtReportLog.log(Status.FATAL, text);
			log.fatal(text);
		} else {
			xtReportLog.log(Status.INFO, code);
			log.info(text);
		}
		Reporter.log(text);
	}

	public void ReporterForLabel(String text, String status) {

		Markup code = MarkupHelper.createLabel(text, ExtentColor.BLUE);
		if (status.equalsIgnoreCase("Pass")) {
			xtReportLog.log(Status.PASS, code);
			log.info(code);
		} else if (status.equalsIgnoreCase("Fail")) {
			xtReportLog.log(Status.FAIL, code);
			log.error(code);
		} else if (status.equalsIgnoreCase("Fatal")) {
			xtReportLog.log(Status.FATAL, text);
			log.fatal(text);
		} else if (status.equalsIgnoreCase("Error")) {
			xtReportLog.log(Status.FATAL, text);
			log.fatal(text);
		} else {
			xtReportLog.log(Status.INFO, code);
			log.info(code);
		}
		Reporter.log(text);
	}

	/**
	 * This method gets data specified in excel
	 * 
	 * @param key
	 * @return String
	 */
	public String getData(String key) {
		return data.get(key);
	}

}
