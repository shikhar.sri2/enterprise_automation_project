package com.main.uiautomation.PageObject;

import com.main.uiautomation.helper.Action.ActionHelper;
import com.main.uiautomation.helper.Dropdown.DropDownHelper;
import com.main.uiautomation.helper.TestBase.TestBase;
import com.main.uiautomation.helper.Wait.WaitHelper;
import com.main.uiautomation.helper.genericHelper.GenericHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.Calendar;
import java.util.logging.Logger;

public class LoginPage extends TestBase {

	public WebDriver driver;
	Logger logger;
	private GenericHelper objGenericHelper;
	private WaitHelper objWaitHelper;
	private DropDownHelper objDropDownHelper;
	private ActionHelper objActionHelper;

	// String browserName;
	public LoginPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		objGenericHelper = PageFactory.initElements(driver, GenericHelper.class);
		objWaitHelper = PageFactory.initElements(driver, WaitHelper.class);
		objDropDownHelper = PageFactory.initElements(driver, DropDownHelper.class);
		objActionHelper = PageFactory.initElements(driver, ActionHelper.class);
	}

	@FindBy(xpath = "//a[@href='/admin/dashboard']//img[@alt='V-Thru']")
	protected WebElement applicationLogo;

	@FindBy(xpath = "//*[@id='username']")
	protected WebElement userNameTextBox;

	@FindBy(xpath = "//*[@id='password']")
	protected WebElement passwordTextBox;

	@FindBy(xpath = "//button[@type='submit']")
	protected WebElement loginButton;

	@FindBy(xpath = "//div[@class='alert alert-danger alert-error']")
	protected WebElement invalidCredentials;

	@FindBy(xpath = "//a[@href='/admin/resetting/request']")
	protected WebElement forgotPasswordLink;

	public boolean verifyLogoIsDisplayed(){
		return objGenericHelper.isDisplayed(applicationLogo, "Application Logo");
	}

	public boolean insertUserName(String value){
		return objGenericHelper.setElementText(userNameTextBox, "Username", value);
	}

	public boolean isDisplayedUserName(String value){
		return objGenericHelper.isDisplayed(userNameTextBox, "Username");
	}

	public boolean insertPassword(String value){
		return objGenericHelper.setElementText(passwordTextBox, "Password", value);
	}

	public boolean clickLoginButton(){
		return objGenericHelper.elementClick(loginButton, "Error Msg");
	}

	public boolean verifyErrorMsgIsDisplayed(){
		return objGenericHelper.isDisplayed(invalidCredentials, "Error Msg");
	}

	public boolean forgotPasswordLink(){
		return objGenericHelper.elementClick(forgotPasswordLink, "Forgot Password link");
	}


}
