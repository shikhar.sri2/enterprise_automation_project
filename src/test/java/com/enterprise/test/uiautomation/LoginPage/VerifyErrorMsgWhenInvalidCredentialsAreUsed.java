package com.enterprise.test.uiautomation.LoginPage;

import com.main.uiautomation.DataProvider.TestDataProvider;
import com.main.uiautomation.PageObject.LoginPage;
import com.main.uiautomation.Utility.ExcelReader;
import com.main.uiautomation.helper.TestBase.TestBase;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

public class VerifyErrorMsgWhenInvalidCredentialsAreUsed extends TestBase {

    private static final Logger log = Logger.getLogger(VerifyErrorMsgWhenInvalidCredentialsAreUsed.class);

    private ExcelReader excelReader;
    private LoginPage loginPage;

    @BeforeClass
    public void setup() throws Exception {
        excelReader = new ExcelReader();
        HashMap fileDetails = new HashMap<String, String>();
        fileDetails.put("FileName", "LoginPage.xlsx");
        fileDetails.put("SheetName", "TC_01");
        excelReader.setFilenameAndSheetName(fileDetails);
    }

    @BeforeMethod
    public void initializeObjects() {
        loginPage = new LoginPage(driver);
    }

    @Test(dataProvider = "TestRuns", dataProviderClass = TestDataProvider.class)
    public void VerifyErrorMsgWhenInvalidCredentialsAreUsed(Map<String, String> data) throws Exception {
        try {
            Assert.assertTrue(loginPage.insertUserName(data.get("UserName")), "Username is inserted successfully");
            Assert.assertTrue(loginPage.insertPassword(data.get("Password")), "Password is inserted successfully");
            Assert.assertTrue(loginPage.clickLoginButton(), "Login Button is clicked successfully");
            Assert.assertTrue(loginPage.verifyErrorMsgIsDisplayed(), "Error msg is displayed");
        } catch (Exception e) {
            e.printStackTrace();
            Reporter.log("Test Case Failed", true);
        }
    }

}
